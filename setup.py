from setuptools import setup

setup(
    name='pokemon',
    version='1.0.0',
    packages=[''],
    url='https://gitlab.com/groups.francesco.racciatti/pokepoke/pokemon',
    license='MIT License',
    author='francescoracciatti',
    author_email='racciatti.francesco@gmail.com',
    description='gRPC service model for PokePoke.'
)
