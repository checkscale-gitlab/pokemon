# Pokemon
gRPC service model for [PokePoke](https://gitlab.com/groups.francesco.racciatti/pokepoke).


# Table Of Contents
- [Compiling proto files](#compiling-proto-files)
  - [Docker](#docker)
  - [Terminal](#terminal)
- [Author](#author)
- [License](#license)


## Compiling proto files
The instructions that follow refers to a Python3 based gRPC client.
For other languages refer to the:
 - [gRPC official docs](https://grpc.io/docs/languages/),
 - [gRPC Docker platforms](https://hub.docker.com/u/grpc).


### Docker
Build the Docker image, then run the container to compile the `pokemon.proto` file. 
The generated protobuf file is located at `/pokemon/pokemon_pb2.py`.
```shell
$ cd path_to/pokemon
pokemon$ docker build . --tag pokemon:latest
pokemon$ docker run --name mycontainer pokemon:latest && docker cp mycontainer:/pokemon/pokemon_pb2.py .
```


### Terminal
Clone the Pokemon project and activate a virtualenv:
```shell
$ git clone https://gitlab.com/groups.francesco.racciatti/pokepoke/pokemon.git
$ cd path_to/pokemon
pokemon$ python3 -m venv ./venv
pokemon$ source venv/bin/activate
```

Install project's requirements and dependencies:
```shell
(venv) pokemon$ pip3 install -r requirements.txt
(venv) pokemon$ pip3 install -e .
```

Then, compile the `pokemon.proto` file:
```shell
(venv) pokemon$ python3 -m grpc_tools.protoc --proto_path=. pokemon.proto --python_out=.
```
It will create the file `pokemon_pb2.py`.


## Author
Francesco Racciatti


## License
This project is licensed under the [MIT license](LICENSE).
